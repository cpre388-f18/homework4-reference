package edu.iastate.cpre388.cpre388_homework4_sampleserver;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.io.IOException;

public class MediaService extends Service {
    /** The notification channel ID for showing the media playback. */
    private static final String CHANNEL_ID = "PlaybackChannel";
    /**
     * The notification ID for the playback notification.  Only one instance of this notification,
     * so a static ID is desired.
     */
    private static final int PLAYBACK_NOTIFICATION_ID = 1;
    /** The tag used for log messages. */
    private static final String TAG = "MediaService";
    /** The message.what to indicate a pause media request. */
    public static final int SERVICE_PAUSE_WHAT = 1;
    /** The message.what to indicate a stop media request. */
    public static final int SERVICE_STOP_WHAT = 2;

    /** The Messenger used for receiving media control messages (pause and stop). */
    private Messenger mMessenger;
    /** The actual media player that this service will control */
    private MediaPlayer mMediaPlayer = null;
    /** A thread that starts media playback asynchronously. */
    private Thread mStartPlaybackThread = null;
    /** The song title of the current playback. */
    private String mSongTitle = "-";

    public MediaService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.v(TAG, "Service created");
        mMessenger = new Messenger(new IncomingHandler(this));
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnCompletionListener(mCompletionListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.v(TAG, "Service destroyed");
        mMessenger = null;
        mMediaPlayer.release();
    }

    /**
     * Instance of a listener to handle the end of media playback.  This stops the service.
     */
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            // Media playback is done.  Stop the service.
            stopForeground(true);
            stopSelf();
        }
    };

    private void handleMediaControlMessage(Message msg) {
        switch (msg.what) {
            case SERVICE_PAUSE_WHAT:
                Log.i(TAG, "Received pause media message.");
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.pause();
                }
                // Go to a background service, but keep the notification.
                stopForeground(false);
                break;
            case SERVICE_STOP_WHAT:
                Log.i(TAG, "Received stop media message.");
                try {
                    mMediaPlayer.stop();
                } catch (IllegalStateException ex) {
                    Log.w(TAG, "Cannot stop media player because its state.");
                    ex.printStackTrace();
                }
                // Go to a background service, if foreground.
                stopForeground(true);
                stopSelf();
                break;
            default:
                Log.w(TAG, "Unknown message.what: " + msg.what);
        }
    }

    // SECTION: BIND

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "Client binding");
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG, "Client unbinding");
        return super.onUnbind(intent);
    }

    /**
     * A handler for receiving media control messages from bound clients.
     */
    private static class IncomingHandler extends Handler {
        private MediaService mListener;

        IncomingHandler(MediaService listener) {
            // Store a reference to the outer class to call functions.  This is needed because
            // IncomingHandler is static.
            mListener = listener;
        }

        @Override
        public void handleMessage(Message msg) {
            // Pass the message from this inner class (IncomingHandler) to the instance of
            // MediaService that controls media playback.
            mListener.handleMediaControlMessage(msg);
        }
    }


    // SECTION: "STARTED" SERVICE

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Do not have permission.  Must request it.");
            PermissionActivity.showPermissionRequestNotification(this, intent.getData());
            // Permission to read media to play is not held, must request it from the user.
            stopSelf();
            return START_NOT_STICKY;
        }
        // Check arguments
        if (intent.getData() == null) {
            Log.e(TAG, "Data was not set on start intent.  Must be URI to media.");
            stopSelf();
            return START_NOT_STICKY;
        }

        Log.i(TAG, "Start command for song: " + intent.getDataString());
        if (mStartPlaybackThread != null) {
            // Playback is already starting.  Try to stop it.
            mStartPlaybackThread.interrupt();
            try {
                mStartPlaybackThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
                stopSelf();
                return START_NOT_STICKY;
            }
        }
        // Thread objects cannot be reused.  A new instance must always be created.
        Thread startPlaybackThread = new Thread(new StartPlaybackRunnable(intent.getData()));
        startPlaybackThread.start();
        return START_REDELIVER_INTENT;
    }

    private class StartPlaybackRunnable implements Runnable {
        /** The URI of the song to start playing when the mediaplayer is ready. */
        private Uri mSongURI;

        /**
         * Constructs a runnable that prepares the MediaPlayer and starts playback.
         *
         * @param songUri the URI of the song to play when ready
         */
        StartPlaybackRunnable(Uri songUri) {
            mSongURI = songUri;
        }

        @Override
        public void run() {
            Log.v(TAG, "Resetting MediaPlayer.");
            mMediaPlayer.reset();
            try {
                Log.v(TAG, "Setting MediaPlayer data source.");
                mMediaPlayer.setDataSource(MediaService.this, mSongURI);
                Log.v(TAG, "Preparing MediaPlayer.");
                mMediaPlayer.prepare();
            } catch (IOException ex) {
                Log.e(TAG, "Error starting media playback.");
                ex.printStackTrace();
                stopSelf();
            }
            Log.v(TAG, "Starting playback.");
            mMediaPlayer.start();

            // Get song title
            MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
            metaRetriever.setDataSource(MediaService.this, mSongURI);
            mSongTitle = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

            goToForeground();
        }
    }

    /**
     * Helper function that creates a notification and puts this service in the foreground.
     */
    private void goToForeground() {
        createNotificationChannel(this);

        // Create a pending intent that launches the stop playback Activity.
        Intent showActivityIntent = new Intent(this, StopPlaybackActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                showActivityIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        // Build a notification to request permission.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder
                .setContentIntent(pendingIntent)
                .setContentTitle(getString(R.string.notification_playback_title_format, mSongTitle))
                .setContentText(getString(R.string.notification_playback_desc))
                .setSmallIcon(R.drawable.play_ico)
                .setAutoCancel(true);

        // Enter foreground and show the notification.
        startForeground(PLAYBACK_NOTIFICATION_ID, builder.build());
    }

    /**
     * Helper function that creates the media playback notification channel.
     */
    private static void createNotificationChannel(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence channelName = c.getString(R.string.perm_playback_channel);
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = c.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }
}
