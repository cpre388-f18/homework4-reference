package edu.iastate.cpre388.cpre388_homework4_sampleserver;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class PermissionActivity extends AppCompatActivity {
    /** The notification channel ID for showing the permission request. */
    private static final String CHANNEL_ID = "PermissionChannel";
    /**
     * The notification ID for the permission request.  Only one instance of this notification, so
     * a static ID is desired.
     */
    private static final int PERMISSION_NOTIFICATION_ID = 1;
    /**
     * The request key for file read permissions.  When returned with granted permission,
     * the service will be started with the song URI on the Intent.
     */
    private static final int PERMISSION_REQUEST_READ = 1;
    /** The tag used for log messages. */
    private static final String TAG = "PermissionActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            String[] permReq = {Manifest.permission.READ_EXTERNAL_STORAGE};
            requestPermissions(permReq, PERMISSION_REQUEST_READ);
        } else {
            Log.i(TAG, "Permission already held.");
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_READ:
                if (permissions.length == 1
                        && permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "User granted permission request.");
                    startServiceAgain();
                    // This Activity is done.  It should exit.
                    finish();
                } else {
                    Toast.makeText(this, R.string.perm_denied, Toast.LENGTH_SHORT).show();
                    Log.w(TAG, "User denied permission request.");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Helper function that starts the service with the song URI specified in the Intent.
     */
    private void startServiceAgain() {
        Uri songUri = getIntent().getData();
        if (songUri == null) {
            Log.w(TAG, "No song URI in the intent to start the service.");
        } else {
            Log.v(TAG, "Starting service again.");
            Intent serviceIntent = new Intent(this, MediaService.class);
            serviceIntent.setData(songUri);
            startService(serviceIntent);
        }
    }

    /**
     * Helper function to show a notification that requests permission and starts the MediaService
     * if the user grants permission.
     *
     * @param c the context to use when sending then notification (MediaService)
     * @param songToPlay the URI of the song to play when the permission is granted
     */
    static void showPermissionRequestNotification(Context c, Uri songToPlay) {
        createNotificationChannel(c);

        // Create a pending intent that launches this Activity with the song URI to pass back to the
        // service when the permission is granted.
        Intent showActivityIntent = new Intent(c, PermissionActivity.class);
        showActivityIntent.setData(songToPlay);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                c,
                0,
                showActivityIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        // Build a notification to request permission.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(c, CHANNEL_ID);
        builder
                .setContentIntent(pendingIntent)
                .setContentTitle(c.getString(R.string.notification_perm_title))
                .setContentText(c.getString(R.string.notification_perm_desc))
                .setSmallIcon(R.drawable.perm_req_ico)
                .setAutoCancel(true);

        // Show the notification.
        NotificationManagerCompat manager = NotificationManagerCompat.from(c);
        manager.notify(PERMISSION_NOTIFICATION_ID, builder.build());
    }

    /**
     * Helper function that creates the permission request notification channel.
     */
    private static void createNotificationChannel(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence channelName = c.getString(R.string.perm_notification_channel);
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH);
            NotificationManager manager = c.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }
    }
}
