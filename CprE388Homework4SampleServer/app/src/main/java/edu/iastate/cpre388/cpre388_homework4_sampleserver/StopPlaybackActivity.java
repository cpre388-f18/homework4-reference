package edu.iastate.cpre388.cpre388_homework4_sampleserver;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class StopPlaybackActivity extends AppCompatActivity {
    /** The messenger for sending the stop message. */
    private Messenger mMessenger = null;
    /** Tag for log messages. */
    private static final String TAG = "StopPlaybackActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_playback);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent serviceIntent = new Intent(this, MediaService.class);
        bindService(serviceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        
        unbindService(mServiceConnection);
    }

    public void onStopClick(View v) {
        // Check that the service is bound
        if (mMessenger == null) {
            Log.e(TAG, "Service is not bound to send stop message.");
            Toast.makeText(this, R.string.not_bound, Toast.LENGTH_LONG).show();
            return;
        }

        Message stopMessage = Message.obtain(null, MediaService.SERVICE_STOP_WHAT);
        try {
            // Send media stop message.
            mMessenger.send(stopMessage);
        } catch (RemoteException e) {
            Log.e(TAG, "Error while sending stop message.");
            e.printStackTrace();
            Toast.makeText(this, R.string.bind_msg_error, Toast.LENGTH_LONG).show();
            return;
        }
        // Stop the service (will only destroy after unbind).
        Intent serviceIntent = new Intent(this, MediaService.class);
        stopService(serviceIntent);
    }
    
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mMessenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mMessenger = null;
        }
    };
}
