package edu.iastate.cpre388.cpre388_homework4_sample;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.net.URI;

public class MainActivity extends AppCompatActivity {
    /** The package name that contains the media service. */
    private static final String SERVICE_PACKAGE
            = "edu.iastate.cpre388.cpre388_homework4_sampleserver";
    /** The full class name of the remote service. */
    private static final String SERVICE_CLASS
            = "edu.iastate.cpre388.cpre388_homework4_sampleserver.MediaService";
    /** The ComponentName used for explicit intents to the service. */
    private static final ComponentName SERVICE_COMPONENT_NAME
            = new ComponentName(SERVICE_PACKAGE, SERVICE_CLASS);
    /**
     * The request key for populating the songs ListView.  When returned with granted permission,
     * it will load longs into ListView.
     */
    private static final int PERMISSION_REQUEST_LIST_VIEW = 1;
    /** The instance of the song ListView. */
    private ListView mSongsListView;
    /** The messenger for sending messages to bound service. */
    private Messenger mServiceMessenger = null;
    /** Tag for log messages. */
    private static final String TAG = "MainActivity";
    /** The message.what to indicate a pause request to the service. */
    private static final int SERVICE_PAUSE_WHAT = 1;
    /** The message.what to indicate a stop request to the service. */
    private static final int SERVICE_STOP_WHAT = 2;
    /** The currently selected song to start. */
    private Uri mSelectedSong = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSongsListView = findViewById(R.id.songsListView);

        mSongsListView.setOnItemClickListener(mSongListViewListener);

        tryPopulateSongListView();
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent bindIntent = new Intent();
        bindIntent.setComponent(SERVICE_COMPONENT_NAME);
        bindService(bindIntent, mConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        unbindService(mConnection);
    }

    /**
     * Checks external storage read permission.  If held, populate the song ListView.  If not held,
     * request permission and populate ListView IF granted.
     */
    private void tryPopulateSongListView() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            // We do not have permission to do this.  Request it and let onRequestPermissionResult()
            // call this function again when permission is held.
            requestMediaPermission();
            return;
        }

        // The rows we care about loading from the ContentProvider.  Note that _ID is required for
        // the ListView.
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.DATA
        };

        ContentResolver resolver = getContentResolver();
        // Query the external songs list for every song with the default sort order.
        Cursor mediaCursor = resolver.query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                null
        );

        // Set the from & to arrays for mapping from the ContentResolver to the song row layout
        // Views.
        String[] listViewFrom = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM
        };
        @IdRes int[] listViewTo = {
                R.id.titleTextView,
                R.id.artistTextView,
                R.id.albumTextView
        };
        // Create an adapter to bridge the ContentResolver's cursor with the ListView.
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                this,
                R.layout.song_row,
                mediaCursor,
                listViewFrom,
                listViewTo,
                0
        );

        // Associate the Adapter with the ListView.
        mSongsListView.setAdapter(adapter);
    }

    /**
     * Requests file read permission from the user, and will load the song ListView later if
     * granted.
     */
    private void requestMediaPermission() {
        String[] mediaPerm = {Manifest.permission.READ_EXTERNAL_STORAGE};
        requestPermissions(mediaPerm, PERMISSION_REQUEST_LIST_VIEW);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_LIST_VIEW:
                if (permissions.length == 1
                        && permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted.  Load the ListView with the granted permission.
                    tryPopulateSongListView();
                } else {
                    // Permission was denied.  Tell the user why the app isn't working.
                    Toast.makeText(this, R.string.perm_denied, Toast.LENGTH_LONG).show();
                }
                break;
            default:
                // This was not a permission requested in this class.  Pass it up to the base class.
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * The event handler for service bind events.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "Service bound");
            mServiceMessenger = new Messenger(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "Service unbound");
            mServiceMessenger = null;
        }
    };


    // SECTION: MEDIA BUTTONS

    /**
     * Play button click event.  Starts the service with selected song.
     *
     * @param v instance of the play button
     */
    public void onPlayClick(View v) {
        if (mSelectedSong == null) {
            Toast.makeText(this, R.string.no_song_selected, Toast.LENGTH_LONG).show();
            return;
        }

        Intent serviceIntent = new Intent();
        serviceIntent.setComponent(SERVICE_COMPONENT_NAME);
        serviceIntent.setData(mSelectedSong);
        startService(serviceIntent);
    }

    /**
     * Pause button click event.  Sends pause message through Messenger.
     *
     * @param v instance of the pause button
     */
    public void onPauseClick(View v) {
        // Check that the service is connected.
        if (mServiceMessenger == null) {
            Log.w(TAG, "Service not connected for pause message.");
            Toast.makeText(this, R.string.not_bound, Toast.LENGTH_LONG).show();
            return;
        }

        Message pauseMessage = Message.obtain(null, SERVICE_PAUSE_WHAT);
        try {
            mServiceMessenger.send(pauseMessage);
        } catch (RemoteException e) {
            Log.e(TAG, "Error on sending pause message via Messenger.");
            e.printStackTrace();
        }
    }

    /**
     * Stop button event click.  Sends stop message through Messenger and stops the service.
     *
     * @param v instance of the stop button
     */
    public void onStopClick(View v) {
        // Stop service (will not take effect until unbinding though).
        Intent serviceIntent = new Intent();
        serviceIntent.setComponent(SERVICE_COMPONENT_NAME);
        stopService(serviceIntent);

        // Stop song playback via Messenger
        if (mServiceMessenger == null) {
            // Service not bound, can't send stop message
            Log.w(TAG, "Service not connected for stop message.");
            Toast.makeText(this, R.string.not_bound, Toast.LENGTH_LONG).show();
        } else {
            Message stopMessage = Message.obtain(null, SERVICE_STOP_WHAT);
            try {
                mServiceMessenger.send(stopMessage);
            } catch (RemoteException e) {
                Log.e(TAG, "Error sending stop message via Messenger.");
                e.printStackTrace();
            }
        }
    }


    /**
     * An event handler for song ListView selection events.
     */
    private AdapterView.OnItemClickListener mSongListViewListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Cursor c = (Cursor) parent.getAdapter().getItem(position);
            int dataIndex = c.getColumnIndex(MediaStore.Audio.Media.DATA);
            // Add the "file://" scheme to create a valid URI from the file path.
            mSelectedSong = Uri.parse("file://" + c.getString(dataIndex));
            Log.i(TAG, "Song selected: " + mSelectedSong.toString());
        }
    };
}
